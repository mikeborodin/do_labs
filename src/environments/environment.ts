// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBULK3_gffI0_Mv_MgsK9oU3GtxfmQnhTM",
    authDomain: "termohub-e81b2.firebaseapp.com",
    databaseURL: "https://termohub-e81b2.firebaseio.com",
    projectId: "termohub-e81b2",
    storageBucket: "termohub-e81b2.appspot.com",
    messagingSenderId: "496057320969"
  }
};
