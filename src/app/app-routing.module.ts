import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Lab1Component } from './lab1/lab1-component/lab1-component.component';
import { Lab2Component } from './lab2/lab2/lab2.component';
import { MainComponent } from './main/main.component';

const routes: Routes = [
  { path: "", pathMatch: 'full', redirectTo: 'home' },
  { path: "main", component: MainComponent },
  { path: "lab1", component: Lab1Component },
  { path: "lab2", component: Lab2Component },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
