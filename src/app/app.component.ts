import { Component, OnInit, NgZone } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { GoogleAuthProvider } from '@firebase/auth-types';
import * as firebase from 'firebase'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  title = 'app';
  user: any
  constructor(private auth: AngularFireAuth, private zone: NgZone) {

  }
  ngOnInit(): void {
    this.auth.auth.onAuthStateChanged((user: firebase.User) => {
      console.log(user);
      this.zone.run(() => {
        this.user = user
      })
    })
  }

  login() {
    let provider = new firebase.auth.GoogleAuthProvider()
    provider.setCustomParameters({
      prompt: 'select_account'
    });
    this.auth.auth.signInWithPopup(provider)
  }
  logout() {
    this.auth.auth.signOut()
  }
}
