import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularFireModule } from 'angularfire2'
import { AngularFireAuthModule } from 'angularfire2/auth'

import { NgxChartsModule } from '@swimlane/ngx-charts';
import { environment } from '../environments/environment'

import {
  MatToolbarModule,
  MatSidenavModule,
  MatButtonModule,
  MatIconModule,
  MatTooltipModule,
  MatCheckboxModule,
  MatRadioModule,
  MatCardModule,
  MatStepperModule,
  MatFormFieldModule,
  MatTabsModule,
} from '@angular/material';

import { Lab1Component } from './lab1/lab1-component/lab1-component.component'
import { MainComponent } from './main/main.component'
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { Lab2Component } from './lab2/lab2/lab2.component';

@NgModule({
  declarations: [
    AppComponent,
    Lab1Component,
    MainComponent,
    Lab2Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatSidenavModule,
    MatButtonModule,
    MatIconModule,
    MatTooltipModule,
    MatCheckboxModule,
    MatRadioModule,
    MatCardModule,
    MatStepperModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    NgxChartsModule,
    MatTabsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
