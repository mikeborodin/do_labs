import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ai-lab1-component',
  templateUrl: './lab1-component.component.html',
  styleUrls: ['./lab1-component.component.scss']
})
export class Lab1Component implements OnInit {

  bIdx = 4
  rIdx = 5
  tab = [
    //a_ij x3 x4 bi, ei
    [2, 3, 1, 0, 9, 0],
    [3, 2, 0, 1, 13, 0],
    [-3, -4, 0, 0, 0, 0]
  ]

  constructor() { }

  ngOnInit() {
    console.log(this.tab);
  }

  iteration = 0
  step() {
    //Input === tab
    console.log(`Step #${this.iteration}`);

    if (!this.isOptimal(this.tab)) {
      console.log(`Not optimal`);
      let mainEl = this.findMainEl(this.tab)

      let fMainEl = { q: 0, s: 1 }
      if (this.iteration == 0 && (mainEl.q != fMainEl.q || mainEl.s != fMainEl.s)) {
        console.error(`Should be`, mainEl);
      }

      this.calcSquareRule(mainEl.q, mainEl.s)
      this.iteration++;
      // this.steps()
    } else {
      console.log(`Optimal`);
      console.log(`Solution: Max:${this.tab[this.tab.length - 1][this.bIdx]}}, x:${this.tab[this.tab.length - 1][0]} y:${this.tab[this.tab.length - 1][1]}`);
    }
  }

  calcSquareRule(q, s) {
    // var rtab = [];
    // for (var i = 0; i < this.tab.length; i++)
    //   rtab[i] = this.tab[i].slice();
    //value-copy old   

    let a_qs = this.tab[q][s]
    this.tab[q][s] = 1
    // b) на місці головного елемента ставимо 1, у провідному стовпчику всі
    // елементи, окрім головного, прирівнюємо до нуля

    for (let i = 0; i < this.bIdx; i++) { // set all in COL to 0
      if (i != s)
        this.tab[q][i] = 0
    }

    console.log(`Tab`);
    console.log(this.tab);
    // c) новий рядок з номером q отримуємо із старого рядка діленням на головний a_qs

    for (let i = 0; i < this.tab[q].length - 1; i++) {
      if (i != s)
        this.tab[q][i] /= a_qs
    }

    //RESTA ELEMENTIV
    for (let i = 0; i < this.tab.length; i++) {
      //+1 to include b (furmula is the same)
      for (let j = 0; j < this.bIdx + 1; j++) {

        if (i != q && j != s) {
          let newA = this.calcA(i, j, q, s)
          console.log(`Chabge ${this.tab[i][j]} to ${newA}`);
          this.tab[i][j] = newA
        }
      }

    }
  }


  //functions: aij= and bi= 
  calcA(i: number, j: number, q: number, s: number): number {
    return this.tab[i][j] - (this.tab[i][s] * this.tab[q][j]) / this.tab[q][s]
  }

  findMainEl(tab: number[][]): { q: number, s: number } {
    let sV: number = Infinity
    let s = -1
    tab[tab.length - 1].forEach((n) => {
      if (n < sV) {
        sV = n
        s++
      }
    })
    console.log(`Min is #${s}`, tab[tab.length - 1]);

    let rs = []

    for (let i = 0; i < tab.length; i++) {
      if (tab[i][s] <= 0) {
        tab[i][this.rIdx] = Infinity
        rs.push(Infinity)
      }
      else if (tab[i][this.bIdx] > 0 && tab[i][s] > 0) {
        tab[i][this.rIdx] = 0
        rs.push(0)

      } else if (tab[i][s] > 0) {
        tab[i][this.rIdx] = Math.abs(tab[i][this.bIdx] / tab[i][s])
        rs.push(Math.abs(tab[i][this.bIdx] / tab[i][s]))
      }
    }


    let rV: number = Infinity
    let q = -1
    rs.forEach((n) => {
      if (rV > n) {
        rV = n
        q++
      }
    })
    console.log(`Main el: ${q} ${s}`);

    return { q: q, s: s }
  }

  isOptimal(tab: number[][]): boolean {
    let res: boolean = true

    tab[tab.length - 1].forEach((num) =>
      res = res && num >= 0
    )
    return res
  }
}
